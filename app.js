const fs = require('fs')
const fileContents = fs.readFileSync('./input.json', 'utf8')
var input = JSON.parse(fileContents)

class Transaction {
	
	constructor(data) {
		this.date = data.date;
		this.user_id = parseInt(data.user_id);
		this.user_type = data.user_type;
		this.transaction_type = data.type;
		this.operation = data.operation;
		this.amount = parseFloat(data.operation.amount);
		this.currency = data.operation.currency;
	}

	getCommission() {
		if(this.transaction_type === "cash_in"){
			return 0.03 / 100;
		}else{
			return 0.3 / 100;
		}
	}

	computeCommission(){

		var commissionCalc = parseFloat(this.getCommission());
		var commissionAmount = this.amount * commissionCalc;

		if(this.transaction_type === "cash_in" && commissionAmount > 5.00){

			return console.log(parseFloat(5.00).toFixed(2));

		}else if(this.transaction_type === "cash_out" && this.user_type === "juridical"){

			commissionAmount = parseFloat(commissionAmount).toFixed(2);

			return console.log(commissionAmount.length < 3 ? commissionAmount + "0" : commissionAmount);

		}else if(this.transaction_type === "cash_out" && this.user_type === "natural"){

			var date = "";
			var getGreaterDate = "";
			var findObj = find(input, "user_id", String(this.user_id))
			findObj = find(findObj, "type", "cash_out");
			var getAmount = 0;
			if(findObj.length > 0){
				for(var i = 0; i<findObj.length; i++){
					if(date === ""){
						date = new Date(findObj[i].date)
					}

					var newDate = new Date(findObj[i].date)

					var timeDiff = Math.abs(newDate.getTime() - date.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

					if(diffDays > 7){
						getGreaterDate = findObj[i].date
						continue;
					}

					getAmount += findObj[i].operation.amount
				}
			}

			if(this.date === getGreaterDate){

				var dateAmount = find(input, "date", getGreaterDate);

				if(dateAmount[0].operation.amount <= 1000){

					return console.log(parseFloat(0.00).toFixed(2))

				}
			}

			if(this.amount > 1000){

				return console.log(parseFloat((this.amount - 1000) * commissionCalc).toFixed(2))

			}else if(getAmount <= 1000 && findObj.length > 0){

				return console.log(parseFloat(0.00).toFixed(2))

			}else{

				return console.log(parseFloat(this.amount * commissionCalc).toFixed(2))

			}
		}

		return console.log(commissionAmount)
	}
}

function printCommission(data){
	for(var i=0;i<data.length; i++){

		let transaction = new Transaction(data[i]);
		transaction.computeCommission();

	}
}

function find(obj, key, value) {
	let result = []
	try {
		if (obj != null) {
			for (let prop in obj) {
				if (obj.hasOwnProperty(prop)) {
					if (obj[prop][key] != null) {
						let item = typeof obj[prop][key] === 'boolean' ? obj[prop][key].toString().toLowerCase() : obj[prop][key].toString().toLowerCase();
						let n = item.search(value.toLowerCase());
						if (n >= 0) {
							result.push(obj[prop]);
						}
					}
				}
			}
		}
		return result;
	} catch (e) {
		console.log(e);
	}
}

printCommission(input);

